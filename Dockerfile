FROM node:21-alpine

COPY . .

CMD [ "node", "./dist/main" ]