import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Product {
  @PrimaryGeneratedColumn()
  id: number;

  @Column('text')
  handle: string;

  @Column('text')
  title: string;

  @Column('text')
  description: string;

  @Column('text')
  sku: string;

  @Column('text')
  grams: string;

  @Column('int')
  stock: number;

  @Column('float')
  price: number;

  @Column({ type: 'float', name: 'compare_price' })
  comparePrice: number;

  @Column('text')
  barcode: string;
}
