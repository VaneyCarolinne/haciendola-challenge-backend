export interface PathVariable {
  id: number;
  username: string;
}

export interface UserBody {
  username: string;
  password: string;
  code: string;
}
