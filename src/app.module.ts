import { Module } from '@nestjs/common';
import { AppController } from './controllers/app.controller';
import { AppService } from './services/app.service';
import { AuthModule } from './modules/auth.module';
import { UserModule } from './modules/user.module';
import { ProductModule } from './modules/product.module';
import { ProductController } from './controllers/product.controller';
import { UserController } from './controllers/user.controller';
import { ConfigModule } from '@nestjs/config';

@Module({
  imports: [ConfigModule.forRoot(), AuthModule, UserModule, ProductModule],
  controllers: [AppController, ProductController, UserController],
  providers: [AppService],
})
export class AppModule {}
