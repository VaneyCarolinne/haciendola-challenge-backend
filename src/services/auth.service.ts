import { Injectable, UnauthorizedException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { UserService } from 'src/services/user.service';
import { encryptHash } from 'src/utils/encryptHash';

@Injectable()
export class AuthService {
  constructor(
    private jwtService: JwtService,
    private userService: UserService,
  ) {}

  bcrypt = require('bcrypt');

  async signIn(
    username: string,
    password: string,
  ): Promise<{ access_token: string }> {
    const user = await this.userService.findOne(username);
    if (!user) {
      throw new UnauthorizedException();
    }
    if (!this.bcrypt.compareSync(password, user.password)) {
      throw new UnauthorizedException();
    }

    const payload = {
      id: encryptHash(user.id.toString()),
      username: encryptHash(user.username),
      name: encryptHash(user.name),
      phone: encryptHash(user.phone),
      email: encryptHash(user.email),
    };
    return {
      access_token: await this.jwtService.signAsync(payload),
    };
  }
}
