import {
  Injectable,
  Inject,
  Logger,
  InternalServerErrorException,
  BadRequestException,
} from '@nestjs/common';
import { Repository } from 'typeorm';
import { Product } from '../entities/product/product.entity';

@Injectable()
export class ProductService {
  private readonly ProductLogger = new Logger(ProductService.name);
  constructor(
    @Inject('PRODUCT_REPOSITORY')
    private productRepository: Repository<Product>,
  ) {}

  async findAll(): Promise<Product[]> {
    return this.productRepository.find();
  }

  async findOne(id: number): Promise<Product> {
    return this.productRepository.findOneBy({ id: id });
  }

  async create(product: Product): Promise<Product> {
    try {
      const newProduct = this.productRepository.create({
        title: product.title,
        stock: product.stock,
        sku: product.sku,
        price: product.price,
        handle: product.handle,
        grams: product.grams,
        description: product.description,
        comparePrice: product.comparePrice,
        barcode: product.barcode,
      });
      return this.productRepository.save(newProduct);
    } catch (error) {
      this.ProductLogger.warn(`(CREATE Product): ${error} `);
      this.ProductLogger.error(JSON.stringify(product));
      throw new InternalServerErrorException(`(GET Product): ${error} `, {
        cause: new Error(`ERROR: ${error}`),
        description: `ERROR: ${error}`,
      });
    }
  }

  async update(product: Product, id: number): Promise<Product> {
    try {
      const updateResult = await this.productRepository.update(id, {
        title: product.title,
        stock: product.stock,
        sku: product.sku,
        price: product.price,
        handle: product.handle,
        grams: product.grams,
        description: product.description,
        comparePrice: product.comparePrice,
        barcode: product.barcode,
      });
      if (updateResult.affected) {
        return product;
      }
    } catch (error) {
      this.ProductLogger.warn(`(UPDATE Product): ${error} `);
      this.ProductLogger.error(JSON.stringify(product));
      throw new BadRequestException(`(UPDATE Product): ${id} `, {
        cause: new Error(
          `ERROR: NO SE LOGRÓ LA ACTUALIZACIÓN DEL PRODUCTO: ${JSON.stringify(product)}`,
        ),
        description: `ERROR: NO SE LOGRÓ LA ACTUALIZACIÓN DEL PRODUCTO: ${JSON.stringify(product)}`,
      });
    }
  }

  async delete(id: number): Promise<Product> {
    const product = await this.findOne(id);
    if (product) {
      try {
        const deleteResult = await this.productRepository.delete(id);
        if (deleteResult.affected) {
          return new Product();
        }
      } catch (error) {
        this.ProductLogger.warn(`(DELETE Product): ${id} `);
        this.ProductLogger.error(JSON.stringify(product));
        throw new BadRequestException(`(DELETE Product): ${id} `, {
          cause: new Error(`ERROR: NO SE PUDO ELIMINAR EL PRODUCTO ${id} `),
          description: `ERROR: NO SE PUDO ELIMINAR EL PRODUCTO ${id} `,
        });
      }
    } else {
      throw new BadRequestException(`(DELETE Product): ${id} `, {
        cause: new Error(`ERROR: NO EXISTE EL PRODUCTO CON ID: ${id} `),
        description: `ERROR: NO EXISTE EL PRODUCTO CON ID: ${id} `,
      });
    }
  }
}
