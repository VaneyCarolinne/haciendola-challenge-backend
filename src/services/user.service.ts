import {
  Injectable,
  Inject,
  BadRequestException,
  Logger,
  InternalServerErrorException,
} from '@nestjs/common';
import { Repository } from 'typeorm';
import { User } from '../entities/user/user.entity';
import { generateRecoveryCode } from 'src/utils/generateCode';
import { sendEmail } from 'src/utils/sendEmail';

@Injectable()
export class UserService {
  constructor(
    @Inject('USER_REPOSITORY')
    private userRepository: Repository<User>,
  ) {}

  private readonly UserLogger = new Logger(UserService.name);

  bcrypt = require('bcrypt');

  async findAll(): Promise<User[]> {
    return this.userRepository.find();
  }

  async findOne(username: string): Promise<User> {
    return this.userRepository.findOneBy({ username: username });
  }

  async create(user: User): Promise<User> {
    try {
      const newUser = this.userRepository.create({
        name: user.name,
        username: user.username,
        password: this.bcrypt.hashSync(user.password, 10),
        email: user.email,
        phone: user.phone,
      });
      return this.userRepository.save(newUser);
    } catch (error) {
      this.UserLogger.warn(`(CREATE User): ${user} `);
      this.UserLogger.error(JSON.stringify(error));
      throw new InternalServerErrorException(`(CREATE User): ${user} `, {
        cause: new Error(`ERROR: ${error}`),
        description: `ERROR: ${error}`,
      });
    }
  }

  async sendRecoveryCode(username: string): Promise<User> {
    const user = await this.findOne(username);
    if (user) {
      const code = generateRecoveryCode().toString();
      try {
        await this.userRepository.update(user.id, {
          recoveryCode: code,
        });
        user.recoveryCode = code;
      } catch (error) {
        this.UserLogger.warn(
          `(SEND RECOVERY CODE User): ${JSON.stringify(username)} `,
        );
        this.UserLogger.error(JSON.stringify(error));
        throw new BadRequestException(
          `(SEND RECOVERY CODE user): ${JSON.stringify(user)} `,
          {
            cause: new Error(
              `ERROR: NO SE LOGRÓ LA ACTUALIZACIÓN DEL CÓDIGO CON EL USUARIO: ${JSON.stringify(user)}`,
            ),
            description: `ERROR: NO SE LOGRÓ LA ACTUALIZACIÓN DEL CÓDIGO: ${JSON.stringify(user)}`,
          },
        );
      }
      sendEmail(user.email, code);
      return user;
    } else {
      return new User();
    }
  }

  async recoveryPassword(
    username: string,
    code: string,
    password: string,
  ): Promise<User> {
    const user = await this.findOne(username);
    if (user) {
      if (code === user.recoveryCode) {
        try {
          const updateResult = await this.userRepository.update(user.id, {
            password: this.bcrypt.hashSync(password, 10),
          });

          if (updateResult.affected) {
            return user;
          }
        } catch (error) {
          this.UserLogger.warn(`(RECOVERY PASSWORD User): ${username} `);
          this.UserLogger.error(JSON.stringify(error));
          throw new BadRequestException(
            `(RECOVERY PASSWORD user): ${JSON.stringify(user)} `,
            {
              cause: new Error(
                `ERROR: NO SE LOGRÓ LA ACTUALIZACIÓN DE LA CONTRASEÑA CON EL USUARIO: ${JSON.stringify(user)}`,
              ),
              description: `ERROR: NO SE LOGRÓ LA ACTUALIZACIÓN DEL CONTRASEÑA: ${JSON.stringify(user)}`,
            },
          );
        }
      } else {
        throw new BadRequestException(
          `(EL CÓDIGO DE RECUPERACIÓN NO ES CORRECTO): ${code} `,
          {
            cause: new Error(
              `ERROR: EL CÓDIGO DE RECUPERACIÓN NO ES CORRECTO: ${code}`,
            ),
            description: `ERROR: EL CÓDIGO DE RECUPERACIÓN NO ES CORRECTO: ${code}`,
          },
        );
      }
    } else {
      return new User();
    }
  }
}
