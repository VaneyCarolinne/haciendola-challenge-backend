import { Module } from '@nestjs/common';
import { DatabaseModule } from '../db/database.module';
import { userProviders } from '../entities/user/user.providers';
import { UserService } from '../services/user.service';

@Module({
  imports: [DatabaseModule],
  providers: [...userProviders, UserService],
  exports: [UserService],
})
export class UserModule {}
