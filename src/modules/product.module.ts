import { Module } from '@nestjs/common';
import { DatabaseModule } from '../db/database.module';
import { productProviders } from '../entities/product/product.providers';
import { ProductService } from '../services/product.service';

@Module({
  imports: [DatabaseModule],
  providers: [...productProviders, ProductService],
  exports: [ProductService],
})
export class ProductModule {}
