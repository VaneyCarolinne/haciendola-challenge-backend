import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import 'reflect-metadata';
import * as assert from 'assert';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const config = new DocumentBuilder()
    .setTitle('MS Haciendola Challenge')
    .setDescription('Haciendola Challenge Backend')
    .setVersion('1.0.0')
    .build();
  const document = SwaggerModule.createDocument(app, config);

  SwaggerModule.setup('api', app, document);

  assert(process.env.PORT, 'La variable PORT no está configurada');
  await app.listen(process.env.PORT);

  assert(
    process.env.SENDGRID_API_KEY,
    'La variable SENDGRID_API_KEY no está configurada',
  );
  assert(process.env.SECRET, 'La variable SECRET no está configurada');
  assert(process.env.IV, 'La variable IV no está configurada');
}
bootstrap();
