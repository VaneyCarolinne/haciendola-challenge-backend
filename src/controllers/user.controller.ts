import { Body, Controller, Get, Param, Post, Put } from '@nestjs/common';
import { UserService } from 'src/services/user.service';
import { User } from 'src/entities/user/user.entity';
import { PathVariable, UserBody } from 'src/dtos/request.dto';

@Controller()
export class UserController {
  constructor(private userService: UserService) {}

  @Get('/Users')
  getUsers(): Promise<User[]> {
    return this.userService.findAll();
  }

  @Get('/Users/:username')
  getUser(@Param() param: PathVariable): Promise<User> {
    return this.userService.findOne(param.username);
  }

  @Post('/Users')
  createUser(@Body() user: User): Promise<User> {
    return this.userService.create(user);
  }

  @Put('/Users/send-recovery-code')
  sendRecoveryCode(@Body() body: UserBody): Promise<User> {
    return this.userService.sendRecoveryCode(body.username);
  }

  @Put('/Users/recovery-password')
  recoveryPassword(@Body() body: UserBody): Promise<User> {
    return this.userService.recoveryPassword(
      body.username,
      body.code,
      body.password,
    );
  }
}
