import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  UseGuards,
} from '@nestjs/common';
import { ProductService } from 'src/services/product.service';
import { Product } from 'src/entities/product/product.entity';
import { PathVariable } from 'src/dtos/request.dto';
import { AuthGuard } from 'src/services/auth.guard';

@Controller()
export class ProductController {
  constructor(private productService: ProductService) {}

  @UseGuards(AuthGuard)
  @Get('/Products')
  getProducts(): Promise<Product[]> {
    return this.productService.findAll();
  }

  @UseGuards(AuthGuard)
  @Get('/Products/:id')
  getProduct(@Param() param: PathVariable): Promise<Product> {
    return this.productService.findOne(param.id);
  }

  @UseGuards(AuthGuard)
  @Post('/Products')
  createProduct(@Body() product: Product): Promise<Product> {
    return this.productService.create(product);
  }

  @UseGuards(AuthGuard)
  @Put('/Products/:id')
  updateProduct(
    @Param() param: PathVariable,
    @Body() product: Product,
  ): Promise<Product> {
    return this.productService.update(product, param.id);
  }

  @UseGuards(AuthGuard)
  @Delete('/Products/:id')
  deleteProduct(@Param() param: PathVariable): Promise<Product> {
    return this.productService.delete(param.id);
  }
}
