export function encryptHash(textToEncrypt: string): string {
  // eslint-disable-next-line @typescript-eslint/no-var-requires
  const crypto = require('crypto');

  const key = crypto
    .createHash('sha256')
    .update(textToEncrypt, 'ascii')
    .digest();
  const cipher = crypto.createCipheriv('aes-256-cbc', key, process.env.IV);
  cipher.update(process.env.SECRET, 'ascii');
  const encrypted = cipher.final('base64');

  return encrypted + '$#+' + key;
}
