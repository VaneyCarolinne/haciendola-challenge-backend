export function sendEmail(email: string, code: string) {
  // eslint-disable-next-line @typescript-eslint/no-var-requires
  const sgMail = require('@sendgrid/mail');
  sgMail.setApiKey(process.env.SENDGRID_API_KEY);
  const msg = {
    to: email,
    from: 'vaneycarolinne.vc@gmail.com',
    subject: 'CÓDIGO PARA RECUPERAR CONTRASEÑA',
    text: 'Hola aquí tiene su código: ' + code,
    html: '<strong> Hola aquí tiene su código: ' + code + '</strong>',
  };
  sgMail
    .send(msg)
    .then(() => {
      console.log('EMAIL ENVIADO!');
    })
    .catch((error) => {
      console.error('ERROR ENVIADO EMAIL: ' + error);
    });
}
