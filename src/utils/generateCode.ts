import { maxRecoveryNum, minRecoveryNum } from './constants';

export function generateRecoveryCode() {
  const min = Math.ceil(minRecoveryNum);
  const max = Math.floor(maxRecoveryNum);
  return Math.floor(Math.random() * (max - min) + min);
}
